TEST CASES FOR BBLOG APPLICATION

FEATURE: FAVORITE ARTICLES
_______________________________________________________________________________________________________________________

USER LOGGED IN SCENARIOS

Preconditions:
- in any browser (eg. CHROME) open https://qa-task.backbasecloud.com/ and fill authentication pop up by username: candidatex and password: qa-is-cool.
- user is logged in application (new registered account)
- user choose "global feed" section instead "your feed"
- application contains at least one added article in global feed
- tested article is not mark as favorite

# TEST 1: Logged user should mark article as a favourite on global feed list #

Steps:
- In first article on the list click on a heart icon on the right side of article area

Expected result:
- User sees that heart area is fill with blue colour
- Number in heart area increased from 0 to 1

# TEST 2: Logged user should mark article as a favourite on article page in main section #

Steps:
- In first article on the list click on the article title
- On the article page in main section click favourite post button

Expected result:
- User sees that the button is fill with blue colour (The same result is in comment section with favorite post button)
- Number in button area increased from 0 to 1 (The same result is in comment section with favorite post button)
- Button text is changed to 'Unfavorite Post' (The same result is in comment section with favorite post button)

# TEST 3: Logged user should mark post as a favourite on post page in comment section #

Steps:
- In first article on the list click on a article title
- On the article page in comment section click favourite post button

Expected result:
- User sees that the button is fill with blue colour (The same result is in main section with favorite post button)
- Number in button area increased from 0 to 1 (The same result is in main section with favorite post button)
- Button text is changed to 'Unfavorite Post' (The same result is in main section with favorite post button)

_______________________________________________________________________________________________________________________

USER NOT LOGGED IN SCENARIOS

Preconditions:
- in any browser (eg. CHROME) open https://qa-task.backbasecloud.com/ and log in by username: candidatex and password: qa-is-cool.
- user is not logged in application
- application contains at least one added post in global feed

# TEST 1: Not logged user should not mark article as a favourite on article page in main section #

Steps:
- In first article on the list click on the article title
- On the article page in main section click favourite post button

Expected result:
- User is redirected to login form

# TEST 2: Not logged user should not mark article as a favourite on article page in comment section #

Steps:
- In first article on the list click on the article title
- On the article page in comment section click favourite post button

Expected result:
- User is redirected to login form