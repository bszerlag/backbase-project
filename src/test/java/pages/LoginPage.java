package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static config.DriverManager.getDriver;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//h1[text()='Sign in']")
    WebElement loginPageTitle;

    @Step("Checking login page")
    public void assertThatIsOnLoginPage() {
        loginPageTitle.isDisplayed();
        assertTrue(getDriver().getCurrentUrl().contains("login"));
        logger.info("SUCCES: Is on login page");
    }
}