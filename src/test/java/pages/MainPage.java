package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static config.DriverManager.getDriver;

public class MainPage extends BasePage {

    @Step("User clicks on post title")
    public void clickOnPostTitle() {
        getFirstPost().findElement(By.xpath("//app-article-list-item//h1")).click();
        logger.info("User clicks on post title");
    }

    private WebElement getFirstPost() {
        return getDriver()
                .findElements(By.cssSelector("app-article-list-item"))
                .get(0);
    }
}