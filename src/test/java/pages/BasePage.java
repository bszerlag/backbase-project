package pages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static config.DriverManager.getDriver;
import static org.openqa.selenium.support.PageFactory.initElements;

public class BasePage {

    Logger logger = LoggerFactory.getLogger(MainPage.class);

    public BasePage() {
        initElements(getDriver(), this);
    }
}