package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ArticlePage extends BasePage {

    @FindBy(css = "div[class='banner'] i[class='ion-heart']")
    WebElement mainFavoritePostButton;

    @FindBy(css = "div[class='article-actions'] i[class='ion-heart']")
    WebElement commentFavoritePostButton;

    @Step("User clicks on favorite post button in main section")
    public void clickOnFavoritePostButtonInMainSection() {
        mainFavoritePostButton.click();
        logger.info("User clicks on favorite post button in main section");
    }

    @Step("User clicks on favorite post button in comment section")
    public void clickOnFavoritePostButtonInCommentSection() {
        commentFavoritePostButton.click();
        logger.info("User clicks on favorite post button in comment section");
    }
}