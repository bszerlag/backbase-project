package tests;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.ArticlePage;
import pages.LoginPage;
import pages.MainPage;

import static config.DriverConfig.initDriverConfig;
import static config.DriverConfig.navigateToPage;
import static config.DriverManager.closeDriver;
import static config.PropertiesReader.getProperties;
import static java.lang.String.format;

public class TestBase {

    private final Logger logger = LoggerFactory.getLogger(TestBase.class);

    static MainPage mainPage = new MainPage();
    static LoginPage loginPage = new LoginPage();
    static ArticlePage articlePage = new ArticlePage();

    @BeforeEach
    void suitSetup(TestInfo testInfo) {
        initDriverConfig();
        navigateToPage(getProperties().baseUrl());
        logger.info(format("### TEST: %s ###", testInfo.getDisplayName()));
    }

    @AfterAll
    static void suitCleanup() {
        closeDriver();
    }
}