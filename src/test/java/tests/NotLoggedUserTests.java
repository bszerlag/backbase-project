package tests;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import org.junit.jupiter.api.Test;

import static io.qameta.allure.SeverityLevel.NORMAL;

public class NotLoggedUserTests extends TestBase {

    @Severity(NORMAL)
    @Description("Not logged user should not mark article as a favourite on article page in main section")
    @Test
    void shouldNotMarkArticleAsFavouriteOnArticlePageInMainSection() {
        //when
        mainPage.clickOnPostTitle();
        articlePage.clickOnFavoritePostButtonInMainSection();

        //then
        loginPage.assertThatIsOnLoginPage();
    }

    @Severity(NORMAL)
    @Description("Not logged user should not mark article as a favourite on article page in comment section")
    @Test
    void shouldNotMarkArticleAsFavouriteOnArticlePageInCommentSection() {
        //when
        mainPage.clickOnPostTitle();
        articlePage.clickOnFavoritePostButtonInCommentSection();

        //then
        loginPage.assertThatIsOnLoginPage();
    }
}