# Homework for BackBase.com

---
## Test cases

Test cases you can find in project directory in file `TEST_CASES.txt`
___

## Test automation

### Java version:  11

To run this project and generate report of all tests you have to clone this repo, go to the project directory and run:

`./mvnw test allure:report`

You will find the report in **target/site/allure-maven-plugin/index.html** file.

INFO: I did not implement scenarios with logged-in user because I had strange error that appears only in automatic test. 
Screen of this error is in project directory - `test_error.png`